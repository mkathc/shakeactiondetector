package com.osiptel.shakeactiondetector;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity implements ShakeDetection.Listener {
    private View view;
    ShakeDetection sd;
    BroadcastReceiver broadcastReceiver;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SensorManager sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sd = new ShakeDetection(this);
        sd.start(sensorManager);

        view = findViewById(R.id.textView);
        view.setBackgroundColor(Color.MAGENTA);
       // broadcastReceiver = new ShakeReceiver();
        broadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
                    Intent intent2 = new Intent(context, ShakeService.class);
                    intent.putExtra("screen_state", true);
                    startService(intent2);
                } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
                    Log.d("LOGGONG", Intent.ACTION_SCREEN_ON);
                }
            }
        };
        ScreenNoti();

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void hearShake(boolean isShake) {
        if(isShake){
            view.setBackgroundColor(Color.MAGENTA);

        }else {
            view.setBackgroundColor(Color.CYAN);

        }
       // Toast.makeText(this, "Don't shake me, bro!", Toast.LENGTH_SHORT).show();

    }

 /*   @Override
    protected void onPause() {
        super.onPause();

        /*if(sd!=null){
            sd.stop();
        }*/
       /* Toast.makeText(this, "on Pause Shake", Toast.LENGTH_SHORT).show();
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }

        if(sd!=null){
            sd.stop();
        }
    }

    public void ScreenNoti(){
        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
        registerReceiver(broadcastReceiver, intentFilter);
    }
}