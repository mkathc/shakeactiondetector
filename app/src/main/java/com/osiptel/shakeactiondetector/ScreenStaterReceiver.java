package com.osiptel.shakeactiondetector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

public class ScreenStaterReceiver extends BroadcastReceiver {
    private boolean screnOff;
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
            Log.d("LOGOFF RECEIVER", Intent.ACTION_SCREEN_OFF);
            screnOff = true;
        } else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
            Log.d("LOGGONG RECEIVER", Intent.ACTION_SCREEN_ON);
            screnOff = false;
        }

        Intent intent2 = new Intent(context, ShakeService.class);
        intent.putExtra("screen_state", screnOff);
        context.startService(intent2);
    }

}